#!/bin/bash

FAILED=0

#iptables
iptables -t mangle -I PREROUTING -d 192.168.1.123 -p tcp -m tcp --dport 11111 -m mac ! --mac-source 00:1c:42:13:02:e8 -m mac ! --mac-source 00:1c:42:43:6a:1f -j MARK --set-mark 110 && \
iptables -t mangle -D PREROUTING -d 192.168.1.123 -p tcp -m tcp --dport 11111 -m mac ! --mac-source 00:1c:42:13:02:e8 -m mac ! --mac-source 00:1c:42:43:6a:1f -j MARK --set-mark 110 >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need iptables"
fi

#sed
touch tmp_123
sed -i "2a 11111" tmp_123 >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need sed"
fi
rm -rf tmp_123

#awk
echo "aaaa bbbb" | awk -F ' ' '{print $1}' >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need awk"
fi

#envsubst
envsubst -h >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need envsubst"
fi

#nohup
nohup --help >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need nohup"
fi

#ip addr
ip addr >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need ip"
fi

#flock
flock -h >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need flock"
fi

#killall
killall -V >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need killall"
fi

#onstat
onstat -V >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need onstat"
fi

#onclean
onclean -V >/dev/null 2>&1
if [ $? -ne 0 ]; then
  FAILED=1
  echo "need onclean"
fi

exit $FAILED
