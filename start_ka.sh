#!/bin/bash

KA_WORK_PATH=$PWD

source ka.conf
if [ $? -ne 0 ]; then
  echo "check ka.conf, file error"
  exit 0
fi

# 环境检查
ENV_TMP="DB_LOCAL_ENV INTERFACE PRIORITY DB_PORT LOCAL_IP PEER_IP VIP VIP_MASK VID LVS_TCP_TIMEOUT FWMARK_NUM PEER_MAC"
WILL_EXIT=0
for i in $ENV_TMP
do
  if [ ! -v "$i" ]; then
    echo "check ka.conf, $i not exists"
    WILL_EXIT=1
  fi
done
if [ $WILL_EXIT -eq 1 ]; then
  exit 0
fi

source $DB_LOCAL_ENV
if [ $? -ne 0 ]; then
  echo "check $DB_LOCAL_ENV, file error"
  exit 0
fi

$KA_WORK_PATH/env_check.sh
if [ $? -ne 0 ]; then
  exit 1
fi

## 检查配置文件中节点数量，节点数量需要大于等于2
NODE_COUNT_TOTAL=1
for i in $PEER_IP; do
  ((NODE_COUNT_TOTAL++))
done
if [ $NODE_COUNT_TOTAL -lt 2 ]; then
  echo "NODE_COUNT_TOTAL < 2"
  exit 1
fi

if [ ! -d $KA_WORK_PATH/run ]; then
## 生成keepalived配置文件
  mkdir $KA_WORK_PATH/run
  echo "generate keepalived.conf"

  # 修改PEER_IP
  PEER_TMP=$PEER_IP
  TMP=""
  for i in $PEER_IP
  do
    TMP="$TMP
    $i"
  done
  PEER_IP=$TMP

  # 生成REAL_SERVER
  TMP="real_server $LOCAL_IP {
    weight 1
    notify_down \"$KA_WORK_PATH/run/real_server_notify.sh $LOCAL_IP down\"
    notify_up \"$KA_WORK_PATH/run/real_server_notify.sh $LOCAL_IP up\"
    TCP_CHECK {
      connect_ip $LOCAL_IP
      connect_port $DB_PORT
      connect_timeout 1
      delay_loop 1
      retry 3
    }
  }"
  for i in $PEER_IP
  do
    TMP="$TMP
  real_server $i {
    weight 1
    notify_down \"$KA_WORK_PATH/run/real_server_notify.sh $i down\"
    notify_up \"$KA_WORK_PATH/run/real_server_notify.sh $i up\"
    TCP_CHECK {
      connect_ip $i
      connect_port $DB_PORT
      connect_timeout 1
      delay_loop 1
      retry 3
    }
  }"
  done
  REAL_SERVER=$TMP

  (
  export KA_WORK_PATH REAL_SERVER
  for i in $ENV_TMP
  do
    export $i
  done
  envsubst <$KA_WORK_PATH/template/keepalived.conf.template> $KA_WORK_PATH/run/keepalived.conf
  )

## 生成vrrp_check.sh脚本
  echo "generate vrrp_check.sh"
  cp $KA_WORK_PATH/template/vrrp_check.sh.template $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "2a KA_WORK_PATH=$KA_WORK_PATH/run" $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "3a DB_LOCAL_ENV=$DB_LOCAL_ENV" $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "4a KA_LOG_PATH=$KA_WORK_PATH/run/vrrp_check.log" $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "5a NODE_PORT=$DB_PORT" $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "6a KA_PEER_ADDR=\"$PEER_TMP\"" $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "7a KA_LOCAL_ADDR=$LOCAL_IP" $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "8a NODE_COUNT_TOTAL=$NODE_COUNT_TOTAL" $KA_WORK_PATH/run/vrrp_check.sh
  sed -i "9a VIP=$VIP" $KA_WORK_PATH/run/vrrp_check.sh
  chmod a+x $KA_WORK_PATH/run/vrrp_check.sh

## 生成notify.sh脚本
  echo "generate notify.sh"
  cp $KA_WORK_PATH/template/notify.sh.template $KA_WORK_PATH/run/notify.sh
  sed -i "2a KA_WORK_PATH=$KA_WORK_PATH/run" $KA_WORK_PATH/run/notify.sh
  sed -i "3a KA_LOG_PATH=$KA_WORK_PATH/run/notify.log" $KA_WORK_PATH/run/notify.sh
  sed -i "4a VIP=$VIP" $KA_WORK_PATH/run/notify.sh
  chmod a+x $KA_WORK_PATH/run/notify.sh

## 生成fwmark.sh脚本
  echo "generate fwmark.sh"
  FWM_START="iptables -t mangle -I PREROUTING -d $VIP -p tcp -m tcp --dport $DB_PORT"
  FWM_STOP="iptables -t mangle -D PREROUTING -d $VIP -p tcp -m tcp --dport $DB_PORT"
  for i in $PEER_MAC
  do
    FWM_TMP="$FWM_TMP -m mac ! --mac-source $i"
  done
  FWM_TMP="$FWM_TMP -j MARK --set-mark $FWMARK_NUM"
  FWM_START="$FWM_START $FWM_TMP"
  FWM_STOP="$FWM_STOP $FWM_TMP"
  (
  export FWM_START FWM_STOP
  envsubst <$KA_WORK_PATH/template/fwmark.sh.template> $KA_WORK_PATH/run/fwmark.sh
  )
  chmod a+x $KA_WORK_PATH/run/fwmark.sh

## 生成real_server_notify.sh
  echo "generate real_server_notify.sh"
  cp $KA_WORK_PATH/template/real_server_notify.sh.template $KA_WORK_PATH/run/real_server_notify.sh
  sed -i "2a KA_WORK_PATH=$KA_WORK_PATH/run" $KA_WORK_PATH/run/real_server_notify.sh
  chmod a+x $KA_WORK_PATH/run/real_server_notify.sh
fi

## 删除run/state, 防止状态不对争夺master
rm -rf $KA_WORK_PATH/run/state
rm -rf $KA_WORK_PATH/run/state_tmp

DB_IS_PRIMARY=$(onstat -g sds|grep "Primary"|grep -v grep|wc -l)
DB_IS_BACKUP=$(onstat -g sds|grep "Local server type: SDS"|grep -v grep | wc -l)

if [ $DB_IS_PRIMARY -eq 1 ]; then
  echo "start master"
  $KA_WORK_PATH/run/fwmark.sh start
  if [ $? -ne 0 ]; then
    echo "fwmark.sh start failed"
    exit 1
  fi
  ## 生成real server state
  TIME=$(date +"%Y-%m-%d_%H:%M:%S")
  for i in $PEER_IP
  do
    echo "up $TIME" > $KA_WORK_PATH/run/${i}_state
  done
  echo "up $TIME" > $KA_WORK_PATH/run/${LOCAL_IP}_state

  nohup $KA_WORK_PATH/keepalived -f $KA_WORK_PATH/run/keepalived.conf -l -D -n > $KA_WORK_PATH/run/keepalived.log 2>&1 &
elif [ $DB_IS_BACKUP -eq 1 ]; then
  echo "start backup"
  sleep 6
  $KA_WORK_PATH/run/fwmark.sh start
  if [ $? -ne 0 ]; then
    echo "fwmark.sh start failed"
    exit 1
  fi

  ip addr add $VIP/32 scope link dev lo label lo:0   
  if [ $? -eq 0 ]; then
    echo "add vip to lo success"
    echo 1 > /proc/sys/net/ipv4/conf/lo/arp_ignore
    echo 2 > /proc/sys/net/ipv4/conf/lo/arp_announce
    echo 1 > /proc/sys/net/ipv4/conf/all/arp_ignore
    echo 2 > /proc/sys/net/ipv4/conf/all/arp_announce
  else
    echo "add vip to lo failed"
  fi

  ## 生成real server state
  TIME=$(date +"%Y-%m-%d_%H:%M:%S")
  for i in $PEER_IP
  do
    echo "up $TIME" > $KA_WORK_PATH/run/${i}_state
  done
  echo "up $TIME" > $KA_WORK_PATH/run/${LOCAL_IP}_state

  nohup $KA_WORK_PATH/keepalived -f $KA_WORK_PATH/run/keepalived.conf -l -D -n > $KA_WORK_PATH/run/keepalived.log 2>&1 &
else
  echo "start none"
fi
